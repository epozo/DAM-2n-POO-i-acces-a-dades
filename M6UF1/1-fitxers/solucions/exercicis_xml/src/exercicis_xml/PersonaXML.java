package exercicis_xml;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.SAXException;

public class PersonaXML {
	// a) Crea un fitxer de nom persones que emmagatzemi diversos objectes
	// Persona.
	private static void creaFitxerPersones(String nomFitxer) throws IOException {		
		List<Persona> persones = new ArrayList<Persona>();
		persones.add(new Persona("Pere", 53));
		persones.add(new Persona("Joan", 26));
		persones.add(new Persona("Maria", 32));
		persones.add(new Persona("Laura", 19));
		
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(nomFitxer+".bin"))) {
			oos.writeObject(persones);
		}
	}

	// b) Agafant com a base el fitxer anterior, crea un document XML adequat utilitzant DOM.
	@SuppressWarnings("unchecked")
	private static void creaFitxerXML(String nomFitxer) throws IOException {
		List<Persona> persones;
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(nomFitxer+".bin"))) {
			// Crea la capçalera del fitxer XML en memòria
			Document document = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder().getDOMImplementation()
					.createDocument(null, "persones", null);
			document.setXmlVersion("1.0");

			// Afegim els objectres persona a l'XML
			persones = (List<Persona>) ois.readObject();
			for (Persona persona : persones) {
				Element elementPersona = document.createElement("persona");
				document.getDocumentElement().appendChild(elementPersona);

				creaElement("nom", persona.getNom(), elementPersona, document);
				creaElement("edat", String.valueOf(persona.getEdat()), elementPersona, document);
			}

			// Escrivim l'XML a un fitxer
			TransformerFactory
					.newInstance()
					.newTransformer()
					.transform(new DOMSource(document),
							new StreamResult(nomFitxer+".xml"));
		} catch (ClassCastException e) { 
			throw new IOException(e);
		} catch (DOMException e) {
			throw new IOException(e);
		} catch (ParserConfigurationException e) {
			throw new IOException(e);
		} catch (ClassNotFoundException e) {
			throw new IOException(e);
		} catch (TransformerConfigurationException e) {
			throw new IOException(e);
		} catch (TransformerException e) {
			throw new IOException(e);
		} catch (TransformerFactoryConfigurationError e) {
			throw new IOException(e);
		}
	}

	private static void creaElement(String dadaPersona, String valor, Element elementPersona,
			Document document) {
		Element elem = document.createElement(dadaPersona);
		Text text = document.createTextNode(valor);
		elementPersona.appendChild(elem);
		elem.appendChild(text);
	}

	// c) Implementa un mètode que permeti llegir el document XML de l'apartat anterior.
	public static void mostraXMLPersones(String nomFitxer) throws IOException {
		Document document;
		try {
			document = DocumentBuilderFactory.newInstance()
					.newDocumentBuilder().parse(nomFitxer+".xml");
			
			System.out.println(document.getDocumentElement().getNodeName());
			NodeList persones = document.getElementsByTagName("persona");

			for (int i = 0; i < persones.getLength(); i++) {
				Node persona = persones.item(i);
				if (persona.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) persona;

					System.out.print("   " + getNode("nom", element));
					System.out.println(" \t" + getNode("edat", element));
				}
			}
		} catch (SAXException e) {
			throw new IOException(e);
		} catch (ParserConfigurationException e) {
			throw new IOException(e);
		}
	}

	private static String getNode(String etiqueta, Element elem) {
		NodeList node = elem.getElementsByTagName(etiqueta).item(0)
				.getChildNodes();
		Node valorNode = (Node) node.item(0);
		return valorNode.getNodeValue();
	}

	public static void generaHTML(String nomFitxer) throws IOException {
		String fullEstil = nomFitxer+".xsl";
		String dadesPersones = nomFitxer+".xml";
		
		try (FileOutputStream fos = new FileOutputStream(nomFitxer+".html")) {
			Source estils = new StreamSource(fullEstil);
			Source dades = new StreamSource(dadesPersones);
			Result resultat = new StreamResult(fos);
			Transformer transformer = 
					TransformerFactory.newInstance().
					newTransformer(estils);
			transformer.transform(dades, resultat);
		} catch (TransformerConfigurationException e) {
			throw new IOException(e);
		} catch (TransformerFactoryConfigurationError e) {
			throw new IOException(e);
		} catch (TransformerException e) {
			throw new IOException(e);
		}
	}
	
	public static void main(String[] args) {
		String nomFitxer = "persones";
		try {
			creaFitxerPersones(nomFitxer);
			creaFitxerXML(nomFitxer);
			mostraXMLPersones(nomFitxer);
			generaHTML(nomFitxer);
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
}
