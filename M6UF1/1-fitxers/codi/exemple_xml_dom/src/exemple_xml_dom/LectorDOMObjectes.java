package exemple_xml_dom;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.*;

public class LectorDOMObjectes {
	private List<Mascota> mascotes = new ArrayList<Mascota>();

	public static void main(String[] args) {
		try {
			LectorDOMObjectes lector = new LectorDOMObjectes();
			lector.llegeixMascotes();
			lector.mostraMascotes();
		} catch (IOException e) {
			System.err.println(e.getMessage());
		}
	}
	
	public void llegeixMascotes() throws IOException {
		try (FileInputStream reader = new FileInputStream("mascotes.xml")) {
			
			DocumentBuilder documentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
			Document document = documentBuilder.parse(reader);

			// document.getDocumentElement().getNodeName() és "mascotes"
			NodeList mascotesNodeList = document.getElementsByTagName("mascota");
			
			for (int i=0; i<mascotesNodeList.getLength(); i++) { // NodeList no implementa Iterable
				Node mascotaNode = mascotesNodeList.item(i);

				if (mascotaNode.getNodeType() == Node.ELEMENT_NODE) {
					Element mascotaElement = (Element) mascotaNode;
					
					mascotes.add(new Mascota(
							getNodeValue("nom", mascotaElement),
							Integer.parseInt(mascotaElement.getAttribute("num_potes")),
							Boolean.parseBoolean(getNodeValue("te_pel", mascotaElement))
					));
				}
			}	
		} catch (IOException | ParserConfigurationException | SAXException e) {
			throw new IOException(e);
		}
	}

	private static String getNodeValue(String etiqueta, Element element) {
		NodeList nodeList = element.getElementsByTagName(etiqueta).item(0).getChildNodes();
		Node node = nodeList.item(0);
		return node.getNodeValue();
	}
	
	public void mostraMascotes() {
		for (Mascota mascota : mascotes)
			System.out.println(mascota);
	}
}
