package mongodb;

import static com.mongodb.client.model.Accumulators.sum;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Filters.gte;
import static java.util.Arrays.asList;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Accumulators;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;

public class ExempleAgregacio {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("zips");
		// Consulta a la línia de comandes del MongoDB:
		/*
		db.zips.aggregate( [
			{ $group:
				{ _id: "$state",totalPop:
					{ $sum: "$pop" }
				}
			},
			{ $match:
				{ totalPop:
					{ $gte: 10000000 }
				}
			}
		] )
		 */
		// Consulta creada amb documents:
		List<Document> pipeline = Arrays.asList(
				new Document("$group",
						new Document("_id", "$state").append("totalPop", 
								new Document("$sum", "$pop")
						)
				),
				new Document("$match",
						new Document("totalPop",
								new Document("$gte", 10000000)
						)
				)
		);
		List<Document> results = coll.aggregate(pipeline)
				.into(new ArrayList<Document>());
		for (Document doc : results) {
			System.out.println(doc.toJson());
		}
		System.out.println();
		
		// Consulta creada amb JSON:
		pipeline = Arrays.asList(
			Document.parse("{$group: {_id: \"$state\", totalPop: { $sum: \"$pop\" }}}"),
		  	Document.parse("{$match: {totalPop:	{$gte: 10000000}}}")
		);
		results = coll.aggregate(pipeline).into(new ArrayList<Document>());
		for (Document doc : results) {
			System.out.println(doc.toJson());
		}
		System.out.println();
		
		// Consulta creada amb Aggregates:
		coll.aggregate(Arrays.asList(
				Aggregates.group("$state", Accumulators.sum("totalPop", "$pop")),
				Aggregates.match(Filters.gte("totalPop", 1000000))
		));
		for (Document doc : results) {
			System.out.println(doc.toJson());
		}
		System.out.println();
		
		// Consulta creada amb Aggregates i imports static:
		coll.aggregate(asList(
				group("$state", sum("totalPop", "$pop")),
				match(gte("totalPop", 1000000))
		));
		for (Document doc : results) {
			System.out.println(doc.toJson());
		}
		System.out.println();
		
		client.close();
	}

}
