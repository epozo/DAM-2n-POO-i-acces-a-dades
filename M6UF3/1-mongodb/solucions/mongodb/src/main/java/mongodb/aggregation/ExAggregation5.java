package mongodb.aggregation;

import static com.mongodb.client.model.Accumulators.avg;
import static com.mongodb.client.model.Aggregates.group;
import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.sort;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Sorts.ascending;

import java.util.Arrays;
import java.util.Scanner;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExAggregation5 {
	private static Student student;

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("students");
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Introdueix el nom d'un estudiant:");
		String name = sc.nextLine();
		// Mostra totes les activitats
		coll.aggregate(Arrays.asList(
			match(eq("name", name)),
			unwind("$scores")
		)).forEach((Document doc) -> {
			Document scores = doc.get("scores", Document.class);
			System.out.println("Activitat de tipus "+scores.getString("type")+" amb nota "+scores.getDouble("score"));
		});
		// Calcula nota final
		boolean first = true;
		coll.aggregate(Arrays.asList(
			match(eq("name", name)),
			unwind("$scores"),
			group(
					new Document("id","$_id").append("name", "$name").append("type", "$scores.type"),
						avg("score", "$scores.score")),
			sort(ascending("_id.id"))
		)).forEach((Document doc)->{
			Document id = doc.get("_id", Document.class);
			if (first) {
				student = new Student(id.getInteger("id"), id.getString("name"));
			}
			String type = id.getString("type");
			double score = doc.getDouble("score");
			if (type.equals("quiz"))
				student.scoreQuiz=score;
			else if (type.equals("homework"))
				student.scoreHomework=score;
			else if (type.equals("exam")) {
				student.scoreExam=score;
			}
		});
		System.out.println(student==null?"No existeix l'estudiant.":student);

		client.close();
		sc.close();
	}

}
