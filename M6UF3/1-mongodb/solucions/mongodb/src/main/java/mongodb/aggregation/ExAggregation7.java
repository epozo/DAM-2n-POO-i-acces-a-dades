package mongodb.aggregation;

import static com.mongodb.client.model.Aggregates.match;
import static com.mongodb.client.model.Aggregates.unwind;
import static com.mongodb.client.model.Filters.eq;

import java.util.Arrays;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExAggregation7 {
	private boolean trobat;

	public static void main(String[] args) {
		ExAggregation7 ex = new ExAggregation7();
		ex.activitatsEstudiant(5);
		ex.activitatsEstudiant(1000);
	}
	
	public void activitatsEstudiant(int studentId) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("test");
		MongoCollection<Document> coll = db.getCollection("grades");
		
		trobat = false;
		System.out.println("Activitats de l'estudiant "+studentId);
		coll.aggregate(Arrays.asList(
			match(eq("student_id", studentId)),
			unwind("$scores")
		)).forEach((Document doc) -> {
			trobat = true;
			Document score = doc.get("scores", Document.class);
			System.out.format("Tipus: %s, nota: %.2f.\n",
				score.getString("type"), score.getDouble("score"));
		});
		if (!trobat) {
			System.out.println("No existeix l'estudiant.");
		}
		
		client.close();
	}

}
