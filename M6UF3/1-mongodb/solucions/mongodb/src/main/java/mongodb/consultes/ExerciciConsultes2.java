package mongodb.consultes;

import static com.mongodb.client.model.Projections.include;
import static com.mongodb.client.model.Sorts.descending;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExerciciConsultes2 {

	public static void main(String[] args) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("zips");
		
		coll.find().sort(descending("pop")).projection(include("pop")).limit(10).forEach((Document doc) -> 
				System.out.println("Zip: "+doc.getInteger("_id")+" - Pop: "+doc.getInteger("pop")));
		client.close();
	}
}
