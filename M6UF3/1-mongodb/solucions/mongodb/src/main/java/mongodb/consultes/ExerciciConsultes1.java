package mongodb.consultes;

import static com.mongodb.client.model.Filters.eq;
import static com.mongodb.client.model.Projections.include;

import java.util.Scanner;

import org.bson.Document;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class ExerciciConsultes1 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Introdueix el nom d'una població: ");
		String city = scanner.nextLine();
		zipsByCity(city);
		scanner.close();
	}

	public static void zipsByCity(String city) {
		MongoClient client = new MongoClient();
		MongoDatabase db = client.getDatabase("exemples");
		MongoCollection<Document> coll = db.getCollection("zips");
		
		coll.find(eq("city", city.toUpperCase())).projection(include()).forEach(
				(Document doc) -> System.out.print(doc.getInteger("_id")+" "));
		client.close();
	}
}
