### Solucions exercicis de format JSON

#### Exercici 1

```json
{
  "catalog": {
    "cd": [
      {
        "title":"Empire Burlesque",
        "artist":"Bob Dylan",
        "country":"USA",
        "company":"Columbia",
        "price":10.90,
        "year":1985
      },
      {
        "title":"Hide your heart",
        "artist":"Bonnie Tyler",
        "country":"UK",
        "company":"CBS Records",
        "price":9.90,
        "year":1988
      }
    ]
  }
}
```

#### Exercici 2

```json
{
  "menu": {
    "id": "file",
    "value": "File",
    "popup": {
      "menuitem": [
        {
          "value": "New",
          "onclick": "CreateNewDoc()"
        },
        {
          "value": "Open",
          "onclick": "OpenDoc()"
        },
        {
          "value": "Close",
          "onclick": "CloseDoc()"
        }
      ]
    }
  }
}
```

#### Exercici 3

```xml
<glossary>
  <title>example glossary</title>
  <GlossDiv>
    <title>S</title>
    <GlossList>
      <GlossEntry ID="SGML" SortAs="SGML">
        <GlossTerm>Standard Generalized Markup Language</GlossTerm>
        <Acronym>SGML</Acronym>
        <Abbrev>ISO 8879:1986</Abbrev>
        <GlossDef>
          <para>A meta-markup language, used to create markup languages such as DocBook.</para>
          <GlossSeeAlso>GML</GlossSeeAlso>
          <GlossSeeAlso>XML</GlossSeeAlso>
        </GlossDef>
      <GlossSee>markup</GlossSee>
    </GlossEntry>
   </GlossList>
  </GlossDiv>
</glossary>
```
