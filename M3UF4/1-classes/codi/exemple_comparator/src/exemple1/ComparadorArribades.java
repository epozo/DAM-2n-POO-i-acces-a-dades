package exemple1;

import java.util.Comparator;

/**
 * Aquest comparador compara dos vols en funció de l'hora d'arribada.
 */
public class ComparadorArribades implements Comparator<VolReal> {

	@Override
	public int compare(VolReal v1, VolReal v2) {
		return v1.getArribada().compareTo(v2.getArribada());
	}

}
