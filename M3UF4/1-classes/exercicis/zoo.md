## El zoo

![Diagrama de classes del zoo](../imatges/zoo.png)

1. Crea la interfície *Esser* amb el mètode *accio*, que retorna una cadena
i rep un objecte de tipus *Zoo*.

2. Crea la classe abstracta *Animal* que implementa la interfície *Esser*.
Crea els mètodes abstractes *mou*, *alimenta* i *expressa*, que retornen una
cadena i reben un objecte de tipus *Zoo*. Implementa el mètode *accio* perquè
triï un nombre aleatori entre 0 i 2 i cridi al mètode mou si treu un 0,
al mètode alimenta si treu un 1 i al mètode expressa si treu un 2.

3. Crea la classe *Zoo*, que conté una llista d'animals i un espectador.

Crea el mètode *mostraAnimal* que retorna un animal de la llista de forma
aleatòria o *null* si no hi ha cap animal.

Crea el mètode *afegeixAnimal* que rep una cadena. Si la cadena rebuda és
"*vaca*" afegirà a la llista d'animals un nou objecte de tipus *Vaca* i si
la cadena rebuda és "*cocodril*" afegirà un objecte de tipus *Cocodril*.

Crea el mètode *suprimeixAnimal* que rep una cadena. Si la cadena rebuda és
"*vaca*" suprimirà de la llista d'animals un objecte de tipus *Vaca*, si
n'hi ha, i si la cadena rebuda és "*cocodril*" suprimirà un objecte de
tipus *Cocodril*.

Sobrecarrega el mètode *suprimeixAnimal* perquè pugui rebre un objecte de
tipus *Animal* i l'elimini de la llista.

Crea el mètode *suprimeixTots* que rep una cadena. Si la cadena rebuda és
"*vaca*" suprimirà de la llista d'animals tots els objectes de tipus *Vaca*
i si la cadena rebuda és "*cocodril*" suprimirà tots els objectes de tipus
*Cocodril*.

Crea el mètode *mira*. Aquest mètode elegeix un nombre aleatori entre 0 i 1.
Si surt zero escriu el resultat obtingut al cridar el mètode *accio*
d'*espectador*. Si surt ú escriu el resultat obtingut al cridar el
mètode *accio* d'un dels animals guardats al vector (que es tria de
forma aleatòria). Si no hi hagués cap animal, es mostraria el text
"Quin zoo més avorrit! No té cap animal".

4. Crea la classe *Vaca* com a classe derivada d'*Animal*. Sobreescriu els
mètodes *mou*, *alimenta* i *expressa*:

*mou* retorna la cadena "Una vaca es posa a dormir".

*alimenta* retorna "Una vaca comença a pastar amb tranquil·litat".

*expressa* retorna "Una vaca fa mu".

5. Crea la classe *Cocodril* com a classe derivada d'*Animal*. Sobreescriu
els mètodes *mou*, *alimenta* i *expressa*:

*mou* retorna la cadena "Un cocodril neda estany amunt estany avall".

*alimenta* crida al mètode *mostraAnimal* del zoo que rep i, si rep
algun animal, crida al mètode *suprimeixAnimal* amb l'animal rebut i
retorna la cadena "Un cocodril es menja una vaca!" o "Un cocodril es menja
un altre cocodril". Si no rep cap animal o bé es rep a ell mateix, retorna
la cadena "Un cocodril busca a qui es pot menjar".

*expressa* retorna la cadena "Un cocodril obre una boca plena de dents".

6. Crea la classe *Espectador*, que implementa *Esser*. Escriu el mètode
*accio* de manera que obtingui un animal de la llista del zoo i retorni
la cadena "Un espectador mira una vaca", "Un espectador mira a un perillós
cocodril" o "Un espectador no sap a on mirar perquè no troba animals", segons
correspongui.

7. Crea la classe *PrincipalZoo* amb un mètode *main* i les següents
característiques:

Afegeix l'atribut *zoo* de tipus *Zoo*.

Funcionament del programa:

Al començar es mostrarà per pantalla el text "El zoo obre les seves portes"
i es demanarà l'entrada per teclat de l'usuari.

L'usuari podrà entrar els següents comandaments:

*afegeix vaca* / *afegeix cocodril*: crida el mètode *afegeixAnimal* del
*zoo* amb la cadena *vaca* o *cocodril* i escriu el text "Ha arribat al zoo
una enorme vaca" o "Ha arribat al zoo un perillós cocodril" per pantalla
segons sigui el cas.

*suprimeix vaca* / *suprimeix cocodril*: crida el mètode *suprimeixAnimal*
del *zoo* amb la cadena seleccionada i escriu el text "S'ha traslladat
una vaca/un cocodril a un altre zoo".

*suprimeix tots vaca* / *cocodril*: crida al mètode *suprimeixTots* del
*zoo* amb la cadena seleccionada i escriu el text "El zoo deixa de tenir
vaques/cocodrils".

*mira*: crida el mètode *mira* del *zoo*.

Un cop processat un comandament, es demanarà una altra entrada per part de
l'usuari i així successivament. El programa acaba quan s'entra la cadena
*surt*.
