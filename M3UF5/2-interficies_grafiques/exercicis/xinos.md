### Els xinos

Els "xinos" és un joc de probabilitat que consisteix a endevinar el nombre
de monedes totals que els jugadors han amagat a les seves mans.

Cada jugador pot amagar 0, 1, 2 o 3 monedes. Llavors, tots diuen quantes
monedes creuen que hi ha en total i qui ho endevina, si hi ha algú, s'emporta
un punt.

Farem un programa per jugar als "xinos" una mica simplificats.

En el programa hi jugaran 4 jugadors, un jugador humà i tres portats per
l'ordinador. No es comptaran punts i, al contrari que en el joc real,
es permetrà dir nombres repetits quan s'intenta endevinar. A més,
l'ordinador jugarà sempre a l'atzar i així el podrem guanyar fàcilment.

El nostre programa tindrà la següent interfície gràfica:

![GUI Xinos](../imatges/xinos1.png)

L'etiqueta de dalt de tot ens informa sempre de la situació del joc. La segona
etiqueta ens dóna instruccions sobre què hem de fer.

Els quatre botons de l'esquerra serveixen per indicar quantes monedes ens
amagarem en cada partida.

A la part central veurem les monedes que amaga cada jugador i, més avall,
les apostes que fa cada jugador per intentar endevinar les monedes totals.

A la part de baix el jugador pot introduir primer les monedes que amaga i
després la seva aposta. El botó del final serveix tant per confirmar la
nostra aposta com per començar una nova partida.

El joc es desenvoluparà en diverses fases:

1. A la primera fase els jugadors amagaren les monedes.

2. A la segona fase cada jugador farà la seva aposta.

3. A la tercera fase es mostrarà les monedes amagades per tots els jugadors
i el jugador, si n'hi ha, que ha encertat.

4. Finalment, es podrà tornar a començar la partida.

Es proporciona un
[codi a mitges](../codi/javafxXinosEnunciat). Cal
localitzar els diversos *TODO* que apareixen en el codi i anar completant
les parts que hi falten segons les indicacions.

Si us encalleu, també podeu consultar la
[solució](../codi/javafxXinos).
