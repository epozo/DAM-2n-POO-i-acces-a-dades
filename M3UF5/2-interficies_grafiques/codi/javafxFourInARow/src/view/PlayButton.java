package view;

import javafx.scene.control.Button;

public class PlayButton extends Button {
	private final int col;

	public PlayButton(int num) {
		super(Integer.toString(num));
		this.col = num;
		setPrefSize(50,50);
	}
	
	public int getCol() {
		return col;
	}
}
