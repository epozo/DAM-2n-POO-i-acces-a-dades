package cerca_binaria.g;

public class Comprova {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		LlistaOrdenada<Integer> l = new LlistaOrdenada<Integer>();
		Integer[] nums = {9,0,8,1,7,2,6,3,5,4,1,5,7,3,9};
		for (int i=0; i<nums.length; i++) {
			l.add(nums[i]);
		}
		mostraLlista(l);
		System.out.println();
		for (int i=0; i<nums.length; i+=2) {
			l.remove(nums[i]);
		}
		mostraLlista(l);
		
		System.out.println();
		
		LlistaOrdenada<String> l2 = new LlistaOrdenada<String>();
		String[] strs = {"dilluns","dimarts","dimecres","dijous","divendres","dissabte","diumenge"};
		for (int i=0; i<strs.length; i++) {
			l2.add(strs[i]);
		}
		mostraLlista(l2);
		System.out.println();
		for (int i=0; i<strs.length; i+=2) {
			l2.remove(strs[i]);
		}
		mostraLlista(l2);
	}
	
	public static void mostraLlista(LlistaOrdenada<?> l) {
		for (int i=0; i<l.size(); i++) {
			System.out.println(l.get(i));
		}
	}

}
