package cerca_binaria.g;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class LlistaOrdenada<T extends Comparable<T>> {
	private List<T> llista = new ArrayList<T>();
	
	public int binarySearch(T element) {
		return Collections.binarySearch(llista, element);
	}
	
	public void add(T element) {
		/*
		 * El mètode Collections.binarySearch, a més de fer la
		 * cerca binària i dir-nos la posició on es troba un element,
		 * si l'element no hi és, en comptes de tornar -1, torna
		 * la posició on s'hauria d'inserir l'element per mantenir la
		 * llista ordenada. Per no confondre el cas en què el troba amb
		 * el cas en què no, en el segon cas ens torna -(punt d'inserció)-1
		 */
		int pos = Collections.binarySearch(llista, element);
		if (pos < 0) pos = -pos - 1;
		pos = Math.abs(pos);
		llista.add(pos, element);		
	}
	
	public T get(int index) {
		return llista.get(index);
	}
	
	public boolean remove(T element) {
		int pos = Collections.binarySearch(llista, element);
		if (pos < 0)
			return false;
		else {
			llista.remove(pos);
			return true;
		}
	}
	
	public int size() {
		return llista.size();
	}
}

/*
 * Les API de Java incorporen les interfícies SortedSet i SortedMap que permeten treballar amb col·leccions
 * d'elements ordenats. El SortedSet és molt similar a la nostra llista LlistaOrdenada, però com tots els
 * Set no accepta elements duplicats. La classe més utilitzada que implementa SortedSet és TreeSet. Els
 * mètodes add, remove, etc. de TreeSet utilitzen sistemes equivalents a la cerca binària.
 * 
 * Per altra banda, hi ha dos mètodes estàtics que implementen la cerca binària: el conjunt
 * Arrays.binarySearch permet utilitzar la cerca binària en vectors estàtics si aquests estan
 * ordenats. El mètode Collections.binarySearch implementa la cerca binària per List, sempre i quan
 * estiguin prèviament ordenades.
 * 
 */