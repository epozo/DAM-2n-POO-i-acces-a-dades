package components_compostos;

import components_abstractes.ComponentCompost;
import components_abstractes.ComponentNau;

public class Hangar extends ComponentCompost {

	public Hangar(int capacitat, int pes) {
		super(capacitat, pes);
		setNom("Hangar");
	}
	
	@Override
	public boolean add(ComponentNau component) {
		if (!(component instanceof Nau))
			throw new IllegalArgumentException("A un hangar només es poden posar naus.");
		return super.add(component);
	}

	@Override
	public int getPotenciaTotal() {
		return getPotencia();
	}
	
	@Override
	public String toString() {
		return getNom()+"; capacitat: "+getCapacitat()+", pes: "+getPes();
	}
}
