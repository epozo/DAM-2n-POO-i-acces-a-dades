package piles;

public class PilaBuidaException extends Exception {
	private static final long serialVersionUID = 1L;

	public PilaBuidaException() {
		super();
	}

	public PilaBuidaException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public PilaBuidaException(String message, Throwable cause) {
		super(message, cause);
	}

	public PilaBuidaException(String message) {
		super(message);
	}

	public PilaBuidaException(Throwable cause) {
		super(cause);
	}
}
