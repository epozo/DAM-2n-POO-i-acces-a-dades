package domino;

import java.util.Collections;
import java.util.Set;

/**
 * Representa un dels jugadors en una partida de domino.
 */
public class Jugador {
	/**
	 * El conjunt de fitxes que el jugador té.
	 */
	private Set<Fitxa> fitxes;
	/**
	 * Nom del jugador
	 */
	private final String nom;
	
	/**
	 * El constructor inicialitza les fitxes del jugador
	 * agafant 7 fitxes del domino que es passa per paràmetre.
	 * 
	 * @param nom  El nom que s'utilitzarà per referir-nos al jugador.
	 * @param domino  El domino del qual s'agafen les fitxes pel jugador.
	 */
	public Jugador(String nom, Domino domino) {
		this.nom = nom;
		/*
		 * TODO: crea el conjunt de fitxes i inicialitza'l
		 * amb 7 fitxes agafades del domino.
		 */
	}
	
	/**
	 * Retorna el nom del jugador.
	 * 
	 * @return nom del jugador.
	 */
	public String getNom() {
		return nom;
	}
	
	/**
	 * Afegeix una fitxa a les fitxes del jugador.
	 * 
	 * @param f  La fitxa a afegir.
	 */
	public void agafa(Fitxa f) {
		fitxes.add(f);
	}
	
	/**
	 * Retorna una vista inmutable del conjunt de fitxes del jugador.
	 * 
	 * @return un conjunt inmutable que conté les fitxes del jugador.
	 */
	public Set<Fitxa> getFitxes() {
		return Collections.unmodifiableSet(fitxes);
	}
	
	/**
	 * Retorna la quantitat de fitxes que té el jugador.
	 *  
	 * @return quantes fitxes té el jugador.
	 */
	public int numFitxes() {
		return fitxes.size();
	}
	
	/**
	 * Indica si el jugador té una fitxa com aquesta.
	 * 
	 * @param f  La fitxa que es vol comprovar si el jugador té.
	 * @return true si el jugador disposa d'una fitxa amb els mateixos
	 * valors que la fitxa f, false en cas contrari.
	 */
	public boolean te(Fitxa f) {
		return fitxes.contains(f);
	}
	
	/**
	 * Elimina una fitxa del conjunt de fitxes del jugador.
	 * 
	 * @param f  La fitxa a eliminar.
	 * @return true si el jugador tenia aquesta fitxa, false si no la tenia.
	 */
	public boolean juga(Fitxa f) {
		return fitxes.remove(f);
	}
	
	/**
	 * Retorna una cadena amb la representació de totes les fitxes
	 * del jugador separades per espais.
	 * 
	 * @return una representació en forma de cadena de les fitxes del jugador.
	 */
	@Override
	public String toString() {
		String s = "";
		for (Fitxa fitxa : fitxes) {
			s+=fitxa.toString()+" ";
		}
		return s;
	}
	
	/**
	 * Retorna la quantitat de punts que sumen totes les fitxes del jugador.
	 * 
	 * @return els punts que sumen les fitxes del jugador.
	 */
	public int getPuntuacio() {
		/*
		 * TODO: completa segons descripció
		 */
		return 0;
	}
}
