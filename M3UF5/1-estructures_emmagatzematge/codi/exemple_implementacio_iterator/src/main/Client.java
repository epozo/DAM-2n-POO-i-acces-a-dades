package main;

import exemple_iterator.ElMeuIterator;
import exemple_iterator.IntegerArrayList;
import exemple_iterator.IntegerLinkedList;
import exemple_iterator.LaMevaList;

public class Client {

	public static void main(String[] args) {
		//LaMevaList ll = new IntegerLinkedList();
		LaMevaList ll = new IntegerArrayList();
		ll.add(2);
		ll.add(4);
		ll.add(6);
		ll.add(8);
		ll.add(10);
		ll.add(12);
		ll.add(14);
		ll.add(16);
		
		met(ll);
	}

	public static void met(LaMevaList ll) {
		ElMeuIterator it = ll.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}
}
