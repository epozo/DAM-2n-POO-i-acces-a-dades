package controller;

import java.sql.SQLException;

import javax.sql.rowset.JdbcRowSet;
import javax.sql.rowset.RowSetFactory;
import javax.sql.rowset.RowSetProvider;

import model.Film;

public class FilmController {
	private JdbcRowSet rowSet = null;

	public FilmController() {
		try {
			RowSetFactory rowSetFactory = RowSetProvider.newFactory();
			rowSet = rowSetFactory.createJdbcRowSet();
			rowSet.setUrl(ConnectionProperties.DB_URL);
			rowSet.setUsername(ConnectionProperties.DB_USER);
			rowSet.setPassword(ConnectionProperties.DB_PASS);
			rowSet.setCommand("SELECT * FROM film");
			rowSet.execute();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	
	private void updateRowSet(Film f) throws SQLException {
		rowSet.updateInt("film_id", f.getFilmId());
		rowSet.updateString("title", f.getTitle());
		rowSet.updateString("description", f.getDescription());
		rowSet.updateInt("release_year", f.getReleaseYear());
		rowSet.updateInt("language_id", f.getLanguageId());
		rowSet.updateInt("rental_duration", f.getRentalDuration());
		rowSet.updateFloat("rental_rate", f.getRentalRate());
		rowSet.updateFloat("replacement_cost", f.getReplacementCost());
		rowSet.updateInt("length", f.getLength());
		//rowSet.updateObject("rating", f.getRating());
	}
	
	private void updateFilm(Film f) throws SQLException {
		f.setFilmId(rowSet.getInt("film_id"));
		f.setTitle(rowSet.getString("title"));
		f.setDescription(rowSet.getString("description"));
		f.setReleaseYear(rowSet.getInt("release_year"));
		f.setLanguageId(rowSet.getInt("language_id"));
		f.setRentalDuration(rowSet.getInt("rental_duration"));
		f.setRentalRate(rowSet.getFloat("rental_rate"));
		f.setReplacementCost(rowSet.getFloat("replacement_cost"));
		f.setLength(rowSet.getInt("length"));
		//f.setRating(rowSet.getObject("rating"));
	}

	public Film create(Film f) {
		try {
			rowSet.moveToInsertRow();
			updateRowSet(f);
			rowSet.insertRow();
			rowSet.moveToCurrentRow();
		} catch (SQLException ex) {
			try {
				rowSet.rollback();
				f = null;
			} catch (SQLException e) {

			}
			ex.printStackTrace();
		}
		return f;
	}

	public Film update(Film f) {
		try {
			updateRowSet(f);
			rowSet.updateRow();
			rowSet.moveToCurrentRow();
		} catch (SQLException ex) {
			try {
				rowSet.rollback();
			} catch (SQLException e) {

			}
			ex.printStackTrace();
		}
		return f;
	}

	public void delete() {
		try {
			rowSet.moveToCurrentRow();
			rowSet.deleteRow();
		} catch (SQLException ex) {
			try {
				rowSet.rollback();
			} catch (SQLException e) {
			}
			ex.printStackTrace();
		}

	}

	public Film moveFirst() {
		Film f = new Film();
		try {
			rowSet.first();
			updateFilm(f);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return f;
	}

	public Film moveLast() {
		Film f = new Film();
		try {
			rowSet.last();
			updateFilm(f);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return f;
	}

	public Film moveNext() {
		Film f = new Film();
		try {
			if (rowSet.next() == false)
				rowSet.previous();
			updateFilm(f);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return f;
	}

	public Film movePrevious() {
		Film f = new Film();
		try {
			if (rowSet.previous() == false)
				rowSet.next();
			updateFilm(f);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return f;
	}

	public Film getCurrent() {
		Film f = new Film();
		try {
			rowSet.moveToCurrentRow();
			updateFilm(f);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return f;
	}
}
