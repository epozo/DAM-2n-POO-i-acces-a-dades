package modificacio_dades;

public class Name {
	public final int position; 
	public final String name;
	public final char sex;
	public final int freq;
	public final double percentage;
	
	public Name(int position, String name, char sex, int freq, double percentage) {
		this.position = position;
		this.name = name;
		this.sex = sex;
		this.freq = freq;
		this.percentage = percentage;
	}
	
	public static Name parseName(String s) {
		String[] parts = s.split(";");
		int pos = Integer.parseInt(parts[0]);
		String name = parts[1];
		char sex = parts[2].charAt(0);
		int freq = Integer.parseInt(parts[3]);
		double perc = Double.parseDouble(parts[4].replace(',', '.'));
		return new Name(pos, name, sex, freq, perc);
	}
}
