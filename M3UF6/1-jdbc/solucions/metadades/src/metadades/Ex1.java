package metadades;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 1. Fes un programa que donat el nom d'un taula de la base de dades utilitzi
 * un objecte DatabaseMetaData per mostrar les claus primàries d'aquesta taula,
 * les claus primàries importades per aquesta taula, i les claus primàries
 * exportades per aquesta taula.
 */
public class Ex1 {
	public static void main(String args[]) {
		if (args.length==1) {
			new Ex1().run(args[0]);
		} else
			System.out.println("Falta el nom de la taula");
	}

	public void run(String taula) {
		try (Connection connection = DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/sakila", "root",
				"super3")) {
			DatabaseMetaData dbmd = connection.getMetaData();
			mostraClausTaula(dbmd, taula);
		} catch (SQLException e) {
			System.err.println(e.getMessage());
		}
	}

	private void mostraClausTaula(DatabaseMetaData dbmd, String nomTaula)
			throws SQLException {
		System.out.println("Claus primàries:");
		try (ResultSet columnes = dbmd.getPrimaryKeys(null, "sakila", nomTaula);) {
			while (columnes.next()) {
				String nom = columnes.getString("COLUMN_NAME");
				String keySeq = columnes.getString("KEY_SEQ");
				String pkName = columnes.getString("PK_NAME");
				System.out.println("Columna: " + nom + " - " + keySeq +
						" - constraint name: " + pkName);
			}
		}
		
		System.out.println("Importades:");
		try (ResultSet columnes = dbmd.getImportedKeys(null, "sakila", nomTaula);) {
			while (columnes.next()) {
				String nom = columnes.getString("PKCOLUMN_NAME");
				String fkName = columnes.getString("FK_NAME");
				String fkTaulaName = columnes.getString("PKTABLE_NAME");
				System.out.println("Columna: " + nom + " - " + fkName +
						" - Taula: " + fkTaulaName);
			}
		}
		
		System.out.println("Exportats:");
		try (ResultSet columnes = dbmd.getExportedKeys(null, "sakila", nomTaula);) {
			while (columnes.next()) {
				String nom = columnes.getString("PKCOLUMN_NAME");
				String fkName = columnes.getString("FK_NAME");
				String fkTaulaName = columnes.getString("FKTABLE_NAME");
				System.out.println("Columna: " + nom + " - " + fkName +
						" - Taula: " + fkTaulaName);
			}
		}
	}
}
